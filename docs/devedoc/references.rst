.. SPDX-License-Identifier: MIT
.. Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
.. Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
.. This file forms part of the 'enijo-connector' project, see the
   project's readme, notes, and other documentation, including the
   license information, for further details.


References
==========


* `Invenio Rest API Description <https://inveniordm.docs.cern.ch/reference/rest_api/>`_

   * :download:`Converted into OpenAPI 3.0.3 <../_downloads/invenio-api.json>`.
   * `Rendered with openapi-generator as 'html' <../_static/openapi-html/index.html>`_.
   * `Rendered with openapi-generator as 'html2' <../_static/openapi-html2/index.html>`_.
   * `Rendered with openapi-generator as 'dynamic-html' <../_static/openapi-dynamic-html/docs/index.html>`_.
   * `Rendered with swagger-ui <../_static/swagger-ui/swagger-ui.html>`_.

* `OpenAPI 3.0.3 Specification <https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.3.md>`_
