.. SPDX-License-Identifier: MIT
.. Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
.. Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
.. This file forms part of the 'enijo-connector' project, see the
   project's readme, notes, and other documentation, including the
   license information, for further details.


Recommentations
===============

Jupyter Notebook regarding License Identifier
---------------------------------------------

For a wide range of source file formats, the current best practice is to
put the machine-actionable SPDX license identifier as the first or
second line in the style of the respective source language's comment.

For example a Python command line script file might start like this::

  #!/usr/bin/env python3
  # SPDX-License-Identifier: EUPL-1.2

Which in this example would declare, that the license applicable to this
source file is matching the
`EUPL-1.2 <https://spdx.org/licenses/EUPL-1.2.html>`_ according to the
`SPDX Matching Guidelines <https://spdx.dev/license-list/matching-guidelines/>`_.

The use of this format is the most widespread, so much so, that we
would call it a non-formalized standard (as with ``.csv``, which
is also a standard without any authoritative formalization).

However, the source file format employed by Jupyter Notebook, ``.ipynb``
does not support comment lines, or any other way of including the
SPDX license identifier in a standardized way. Machines would need to
have an application-specific parser for the ipynb file format readily
available, and would need to correctly identifiy the specific file
format used (which very much *could* change between major versions of
Jupyter Notebook). Then, a SPDX license identifier could be placed
in a format-specific place, such as through a dedicated "license" cell
as the very first cell, or in the metadata section of the ``.ipynb``
file format.

**Recommendation** We recommend to align the Jupyter Notebook file
format such, that including the SPDX license identifier in the
standardized way (as the first or second line in the style of a comment)
is possible. This SPDX comment could be also made editable by the
graphical user-interface of Jupyter Notebook.

We think transitioning to the ``YAML-1.2`` format (from the currently
used JSON format) would be the most straightforward, as YAML-1.2 is in
fact a superset of JSON, and therefore only the parser itself would
need to be adapted with the rest of the application receiving previous
behaviour out of the persistence layer. Also a file format colloquially
referred to as "json with comments" would fall under this category.

If browser's cross-origin policies pose a problem, a second alternative
would be to transition to javascript format, by leaving the current file
json format as-is, but prefixing it with ``const ipynb = `` and
suffixing it with ``;``. Then the SPDX license identifier could be
included in the form of a javascript command line at the beginning.


Invenio RDM JSON format
-----------------------

1. date-time format

   According to the Rest Api documentation, the json format currently
   does follow RFC3339. Specifically, the "T" and trailing "Z" are
   missing.

   As RFC3339 is the widely recommended profile of ISO 8601, it would
   be interesting to scrutenize the reasoning behind this choice of
   date-time format and evaluate possibilities to transition to the
   usuale date-time format.

   Note, that the documented date-time format is "ISO 8601". And RFC3339
   is a profile, that is a subset, of ISO 8601.

2. draft record property ``expires_at``

   The meaning of the property ``expires_at`` in the draft record
   object is not really explained. This property is present in the
   examples.


python-openapi-client
---------------------

Missing features should be added there:

- Reference objects are currently not supported in ``responses`` or
  ``requestBody`` properties of an operation.
- If the remote should return a json response that does not validate,
  a more informative exception should be thrown. Currently, just a
  KeyError exception is thrown if for example some required property
  is missing.
- The license statement should more clearly address the license
  situation of the default template, and code generated from it.
- file content upload via PUT without multiform, the process used by
  InvenioRdm, is currently not supported by python-openapi-client.
  Support could and should be added.
