.. SPDX-License-Identifier: MIT
.. Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
.. Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
.. This file forms part of the 'enijo-connector' project, see the
   project's readme, notes, and other documentation, including the
   license information, for further details.


Coding Style and Guidelines
===========================

License Identifier
------------------

In order to make the license of the work, and each file, machine-
parseable, each source file must include the SPDX license identifier at
the earliest possible line. For example immediately after the
``#!INTERPRETER`` interpreter line.

Style of the SPDX license identifier to be used::

  .py: # SPDX-License-Identifier: <SPDX License Expression>
  .rst: .. SPDX-License-Identifier: <SPDX License Expression>
  .yaml: # SPDX-License-Identifier: <SPDX License Expression>
  .html: <!-- SPDX-License-Identifier: <SPDX License Expression> -->

For jupyter notebook's ``.ipynb`` files, as it is a pure JSON format,
there currently exists no clearly machine-pareseable way to include the
license notice or identifier. This documentation recommends to include
a separate license-identifier markup section as the very first section
of the jupyter notebook.

At this time, the only used SPDX license expression is a plain license
identifier, and the only license identifier presently in use throughout
this project is ``MIT``. This "MIT" license was retrieved from the
SPDX license list through url https://spdx.org/licenses/MIT
and the placeholder (red) text adapted accordingly. Then, the resulting
effective MIT license has been placed into the ``LICENSE`` file in the
project's root.

.. comment: see also the ipynb/jupyter-notebook related recommendation
   in chapter "recommendations" of this documentation.

.. note::
    The SPDX license identifier helps in identifying an applicable
    license, it does not help in complying with the terms of a license.

.. warning::
    The SPDX license identifier is a means for grouping similar
    licenses, but does not definitely identify the exact wording of a
    license. For further information, see
    `SPDX Matching Guidelines <https://spdx.dev/license-list/matching-guidelines/>`_.
    For example, at https://spdx.org/licenses/MIT *blue* license text
    can be omitted, and *red* license text is replaceable for the
    purpose of checking SPDX license identifiers. The SPDX license
    matching/grouping mechanism does **not indicate equivalency** from
    a legal perspective, however it expedites that determination.


Python Doc-Strings
------------------

Doc-strings within the python sources should follow the
https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html

That way, they will be nicely rendered as part of the technical
documentation.
