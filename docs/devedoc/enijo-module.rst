.. SPDX-License-Identifier: MIT
.. Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
.. Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
.. This file forms part of the 'enijo-connector' project, see the
   project's readme, notes, and other documentation, including the
   license information, for further details.


****************************
 Enijo Module Documentation
****************************

.. automodule:: enijo.connector
   :members:
   :exclude-members: EnijoConnector

EnijoConnector
==============

.. autoclass:: enijo.connector.EnijoConnector
   :members:

Direct Interface
================

DataRepositoryConnector
-----------------------

.. autoclass:: enijo.connector.data_repository_connector.DataRepositoryConnector
   :members:

DraftFileConnector
------------------

.. autoclass:: enijo.connector.draft_file_connector.DraftFileConnector
   :members:

DraftRecordConnector
--------------------

.. autoclass:: enijo.connector.draft_record_connector.DraftRecordConnector
   :members:

Exceptions
----------

.. automodule:: enijo.connector.exceptions
   :members:

FileConnector
-------------

.. autoclass:: enijo.connector.file_connector.FileConnector
   :members:


RecordConnector
---------------

.. autoclass:: enijo.connector.record_connector.RecordConnector
   :members:
