#!/bin/bash
# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.
#
set -eu -o pipefail
MYDIR="$(realpath "$(dirname "$0")")"

cd "$MYDIR"
docker run -p 8888:8888 enijo-connector-jupyter:latest
