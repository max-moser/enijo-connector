#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.

"""
This command line script converts the given yaml file into equivalent
json format.
"""

import argparse
import json
import logging
import sys
import yaml


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("YAML_FILE")
parser.add_argument("JSON_FILE")
ARGS = parser.parse_args()


with open(ARGS.YAML_FILE, mode='r', encoding="UTF-8") as infp, \
     open(ARGS.JSON_FILE, mode='w', encoding="UTF-8") as outfp:
    data = yaml.safe_load(infp)
    json.dump(data, fp=outfp, indent=4)
