#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.

"""
This command line script sets the version at all relevant places in
this project to the specified value.
"""

import argparse
import logging
import os
import re
import sys
import tempfile


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("VERSION", help="The version to be set, for "
     "example ``0.1.0-alpha.1``. Usage of Semver 2.0 "
     "(see https://semver.org) compatible version strings is "
     "recommended.")
ARGS = parser.parse_args()

os.chdir(os.path.join(os.path.dirname(__file__), ".."))


# change the version stored in file ``setup.cfg``
# TODO: find documentation on how to programmatically parse and edit
# the setup.cfg file (of-course, without changing any comments in the
# file and leaving the general formatting in place)
#
# For atomicity, write new file content to temporary file and then
# move the file in place.
#
# read and write in binary mode, in order to not change any property
# of setup.cfg appart from the sought after change. Therefore cannot
# use readline(). In the future, careful consideration of available
# libraries might reveal performant implementation from trusted
# groups or persons.

setup_version_rex = re.compile(b"^(?P<head>version *= *)(?P<version>.+)$")
def change_setup_version(line):
     match = setup_version_rex.match(line)
     if match:
          return match.group("head") + ARGS.VERSION.encode('utf-8')
     else:
          return line

docs_conf_version_rex = re.compile(b"^(?P<head>release *= *)(?P<quote1>['\"])(?P<release>.+)(?P<quote2>['\"])$")
def change_docs_version(line):
     match = docs_conf_version_rex.match(line)
     if match:
          return match.group("head") + b"'" + ARGS.VERSION.encode('utf-8') + b"'"
     else:
          return line

line_rex = re.compile(b"^(?P<line>.*)$")
def line_edit_binary_file(filepath, edit_function):
     dirpath = os.path.dirname(filepath)
     filename = os.path.basename(filepath)
     if "" == dirpath:
          dirpath = "."
     (tmpfh, tmpfile) =  tempfile.mkstemp(dir=dirpath, prefix=f'.{filename}~')
     tmpfp = os.fdopen(tmpfh, mode='rb+')
     # performance? a topic for a later time
     buf = bytearray(1)
     with open(filepath, mode='rb+') as infp:
          ineof = False
          while not ineof:
               # file starts at the beginning of a line
               line = bytearray(0)
               while True:
                    c = infp.readinto1(buf)
                    if 0 == c:
                         ineof = True
                         break
                    line.append(buf[0])
                    if 0x0a == buf[0]: # newline
                         break
               match = line_rex.match(line)
               trail = line[len(match.group(0)):]
               line = edit_function(match.group('line')) + trail
               tmpfp.write(line)
     tmpfp.close()
     os.rename(tmpfile, filepath)

line_edit_binary_file("setup.cfg", change_setup_version)
line_edit_binary_file("docs/conf.py", change_docs_version)
