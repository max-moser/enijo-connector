#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.

"""
This command line script takes the given primary json file and merges it
with all files recursively found inside the given directory.

Result is written to sys.stdout as json.

Although this tool is intended to keep an OpenAPI Description in
multiple separate files for maintainable and merge them together for
actual usage, the merge logic itself operates on the json format itself
without regard if it's openapi or not, except where noted.

Note in YAML: this script is not intended to be used on yaml files.
However, if the input files are yaml, they are tried to be converted
to json and processing continues if it succeeds. Note, that some
features of yaml might not be supported.
"""

import argparse
import json
import logging
import os
import os.path
import sys
import yaml


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("MAIN_FILE", help="the main json file")
parser.add_argument("MERGE_DIRECTORY",
     help="files from this directory are merged into the main json file")
ARGS = parser.parse_args()


with open(ARGS.MAIN_FILE, mode='r', encoding="UTF-8") as infp:
     # Note: load always with yaml, which should also parse json just
     # fine.
     data = yaml.safe_load(infp)

def ensure_key_exists(key, data):
     d = data
     for k in key:
          if k not in d:
               d[k] = { }
          d = d[k]

def merge_sublist(data, subdata):
     # for each item in subdata: if item is already part of data
     # according to '==' comparison, don't append.
     # but if the item in subdata is not yet part of data, then
     # append them in order.
     for item in subdata:
          if item not in data:
               data.append(item)

def merge_subdata(data, subdata):
     """
     merge two dict. the parameter `data` is modified.
     """
     for key in subdata:
          if key not in data:
               data[key] = subdata[key]
          else:
               if type(data[key]) != type(subdata[key]):
                    raise Exception("key={}: cannot merge subdata[key] "
                         "of type {} into data[key] of type {}.",
                         key, type(data[key]), type(subdata[key]))
               seqtypes = (list, tuple)
               if isinstance(data[key], seqtypes):
                    merge_sublist(data[key], subdata[key])
               else:
                    merge_subdata(data[key], subdata[key])

def merge_subdata_at_key(data, key, subdata):
     # descend into data, as anything above key shall not be changed.
     for k in key:
          data = data[k]
     # use name `key` for other purposes
     del key

     # merge two dicts recursively
     merge_subdata(data, subdata)

for (dirpath, dirnames, filenames) in os.walk(ARGS.MERGE_DIRECTORY, onerror=True):
     dirpath = os.path.relpath(dirpath, ARGS.MERGE_DIRECTORY)
     
     # directory name to key conversion
     # in the future, could use punycode to enable encoding of any
     # key into directory names.
     # Note: empty key is possible in json, but cannot be encoded at
     # the moment (empty directory name not allowed by most filesystems)
     key = dirpath.split("/")

     # ensure data has node at this location, if any files are to be
     # merged.
     if 0 < len(filenames):
          ensure_key_exists(key, data)

     # Note: filenames are actually not used to locate where the merge
     # happens. however, directory names are used.
     for filename in filenames:
          filepath = os.path.join(ARGS.MERGE_DIRECTORY, dirpath, filename)
          with open(filepath, mode='r', encoding="UTF-8") as infp:
               # Note: load always with yaml, which should also parse
               # json just fine.
               subdata = yaml.safe_load(infp)
          merge_subdata_at_key(data, key, subdata)

json.dump(data, fp=sys.stdout, indent=4)
