from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import cast, List
from typing import Union
from ..types import UNSET, Unset




T = TypeVar("T", bound="RecordNewFiles")

@attr.s(auto_attribs=True)
class RecordNewFiles:
    """  """
    enabled: bool
    default_preview: Union[Unset, str] = UNSET
    order: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)


    def to_dict(self) -> Dict[str, Any]:
        enabled = self.enabled
        default_preview = self.default_preview
        order: Union[Unset, List[str]] = UNSET
        if not isinstance(self.order, Unset):
            order = self.order





        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({
            "enabled": enabled,
        })
        if default_preview is not UNSET:
            field_dict["default_preview"] = default_preview
        if order is not UNSET:
            field_dict["order"] = order

        return field_dict



    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        enabled = d.pop("enabled")

        default_preview = d.pop("default_preview", UNSET)

        order = cast(List[str], d.pop("order", UNSET))


        record_new_files = cls(
            enabled=enabled,
            default_preview=default_preview,
            order=order,
        )

        record_new_files.additional_properties = d
        return record_new_files

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
