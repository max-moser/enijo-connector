""" A client library for accessing InvenioRDM Rest API """
from .client import AuthenticatedClient, Client