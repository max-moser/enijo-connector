from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import Dict
from typing import cast
from ...models.record_new import RecordNew
from ...models.record_read import RecordRead



def _get_kwargs(
    *,
    client: Client,
    json_body: RecordNew,

) -> Dict[str, Any]:
    url = "{}/records".format(
        client.base_url)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    

    

    

    json_json_body = json_body.to_dict()



    

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "json": json_json_body,
    }


def _parse_response(*, response: httpx.Response) -> Optional[RecordRead]:
    if response.status_code == 201:
        response_201 = RecordRead.from_dict(response.json())



        return response_201
    return None


def _build_response(*, response: httpx.Response) -> Response[RecordRead]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    json_body: RecordNew,

) -> Response[RecordRead]:
    kwargs = _get_kwargs(
        client=client,
json_body=json_body,

    )

    response = httpx.post(
        **kwargs,
    )

    return _build_response(response=response)

def sync(
    *,
    client: Client,
    json_body: RecordNew,

) -> Optional[RecordRead]:
    """  """

    return sync_detailed(
        client=client,
json_body=json_body,

    ).parsed

async def asyncio_detailed(
    *,
    client: Client,
    json_body: RecordNew,

) -> Response[RecordRead]:
    kwargs = _get_kwargs(
        client=client,
json_body=json_body,

    )

    async with httpx.AsyncClient() as _client:
        response = await _client.post(
            **kwargs
        )

    return _build_response(response=response)

async def asyncio(
    *,
    client: Client,
    json_body: RecordNew,

) -> Optional[RecordRead]:
    """  """

    return (await asyncio_detailed(
        client=client,
json_body=json_body,

    )).parsed
