# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation for further details.

import setuptools

setuptools.setup()
